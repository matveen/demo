APP DEMO
===================
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/bdb842c5-891b-4a17-bc0b-9b72b5aa2340/small.png)](https://insight.sensiolabs.com/projects/bdb842c5-891b-4a17-bc0b-9b72b5aa2340)


Thème : adminLTE 2 `https://almsaeedstudio.com/`

## Installation

Pré-requis : 
 - PHP v.7
 - GIT
 - Composer
 - Node && npm && bower
 - Gulp

#### Base de données
- Créer la base de données `spc`

#### Vendors
- Installer composer et lancer depuis la racine du projet la commande : `composer install`

#### Permissions

```sh
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/sessions web/media web/uploads
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/sessions web/media web/uploads
```
#### Migrations

Lancement des migrations avec la commande `php bin/console doctrine:migrations:migrate`

#### Créer un utilisateur

`http://symfony.com/doc/current/bundles/FOSUserBundle/command_line_tools.html`

- `php bin/console fos:user:create adminuser --super-admin`

#### ASSETS, ressources graphiques

lancer `npm install`

lancer `bower install`

Les assets sont gérés avec Gulp depuis le fichier à la racine gulpfile.js
Lancer la commande `gulp` ou en production `npm run release`
Les assets gérés automatiquement lors du déploiement.

#### COMMANDES

<VirtualHost *:80>
    ServerName www.spc.local
    ServerAlias spc.local

    DocumentRoot /home/mdeglas/www/spc/web
    <Directory /home/mdeglas/www/spc/web>
        # enable the .htaccess rewrites
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/mdeglas/www/spc/var/logs/error.log
    CustomLog /home/mdeglas/www/spc/var/logs/access.log combined
</VirtualHost>
