<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170706122700 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fos_users (id INT AUTO_INCREMENT NOT NULL, avatar_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', gender VARCHAR(10) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, firstname VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, birthday DATE NOT NULL, endDateSubscription DATE DEFAULT NULL, phone_mobile VARCHAR(45) NOT NULL, information LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_32427CF792FC23A8 (username_canonical), UNIQUE INDEX UNIQ_32427CF7A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_32427CF7C05FB297 (confirmation_token), UNIQUE INDEX UNIQ_32427CF786383B10 (avatar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_6CE8ED145E237E06 (name), UNIQUE INDEX UNIQ_6CE8ED14989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_avatars (id INT AUTO_INCREMENT NOT NULL, file VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event (id INT AUTO_INCREMENT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, address_id INT DEFAULT NULL, categori_id INT DEFAULT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, date_start DATETIME NOT NULL, date_end DATETIME DEFAULT NULL, date_to_published DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, enabled TINYINT(1) DEFAULT \'0\' NOT NULL, nb_limit_players INT DEFAULT NULL, is_private TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_ED7D876A989D9B62 (slug), INDEX IDX_ED7D876ADE12AB56 (created_by), INDEX IDX_ED7D876A16FE72E1 (updated_by), INDEX IDX_ED7D876AF5B7AF75 (address_id), INDEX IDX_ED7D876A425FCA7D (categori_id), INDEX IDX_ED7D876AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_address (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, street_name VARCHAR(255) NOT NULL, number INT NOT NULL, complement VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(5) NOT NULL, city VARCHAR(255) NOT NULL, latitude NUMERIC(18, 12) DEFAULT NULL, longitude NUMERIC(18, 12) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_coverages (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, resume LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_69547E1D71F7E88B (event_id), INDEX IDX_69547E1DDE12AB56 (created_by), INDEX IDX_69547E1D16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_participations (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_223C541671F7E88B (event_id), INDEX IDX_223C5416A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fos_users ADD CONSTRAINT FK_32427CF786383B10 FOREIGN KEY (avatar_id) REFERENCES app_avatars (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876ADE12AB56 FOREIGN KEY (created_by) REFERENCES fos_users (id)');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876A16FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_users (id)');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876AF5B7AF75 FOREIGN KEY (address_id) REFERENCES app_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876A425FCA7D FOREIGN KEY (categori_id) REFERENCES app_categories (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876AA76ED395 FOREIGN KEY (user_id) REFERENCES fos_users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE app_coverages ADD CONSTRAINT FK_69547E1D71F7E88B FOREIGN KEY (event_id) REFERENCES app_event (id)');
        $this->addSql('ALTER TABLE app_coverages ADD CONSTRAINT FK_69547E1DDE12AB56 FOREIGN KEY (created_by) REFERENCES fos_users (id)');
        $this->addSql('ALTER TABLE app_coverages ADD CONSTRAINT FK_69547E1D16FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_users (id)');
        $this->addSql('ALTER TABLE app_participations ADD CONSTRAINT FK_223C541671F7E88B FOREIGN KEY (event_id) REFERENCES app_event (id)');
        $this->addSql('ALTER TABLE app_participations ADD CONSTRAINT FK_223C5416A76ED395 FOREIGN KEY (user_id) REFERENCES fos_users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876ADE12AB56');
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876A16FE72E1');
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876AA76ED395');
        $this->addSql('ALTER TABLE app_coverages DROP FOREIGN KEY FK_69547E1DDE12AB56');
        $this->addSql('ALTER TABLE app_coverages DROP FOREIGN KEY FK_69547E1D16FE72E1');
        $this->addSql('ALTER TABLE app_participations DROP FOREIGN KEY FK_223C5416A76ED395');
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876A425FCA7D');
        $this->addSql('ALTER TABLE fos_users DROP FOREIGN KEY FK_32427CF786383B10');
        $this->addSql('ALTER TABLE app_coverages DROP FOREIGN KEY FK_69547E1D71F7E88B');
        $this->addSql('ALTER TABLE app_participations DROP FOREIGN KEY FK_223C541671F7E88B');
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876AF5B7AF75');
        $this->addSql('DROP TABLE fos_users');
        $this->addSql('DROP TABLE app_categories');
        $this->addSql('DROP TABLE app_avatars');
        $this->addSql('DROP TABLE app_event');
        $this->addSql('DROP TABLE app_address');
        $this->addSql('DROP TABLE app_coverages');
        $this->addSql('DROP TABLE app_participations');
    }
}
