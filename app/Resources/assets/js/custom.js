$(function () {

    var dateToday = new Date();

    $('[data-toggle="popover"]').popover();

    $('.datepicker').datetimepicker({
      format:'d/m/Y',
      timepicker:false,
      dayOfWeekStart : 1,
      lang: 'fr',
      scrollMonth: false,
      scrollTime: false,
      scrollInput: false
    });

    $('.datepickerPast').datetimepicker({
      format:'d/m/Y',
      timepicker:false,
      dayOfWeekStart : 1,
      lang: 'fr',
      maxDate:0,
      scrollMonth: false,
      scrollTime: false,
      scrollInput: false
    });

    $('.datetimepicker').datetimepicker({
      format:'d/m/Y H:i',
      dayOfWeekStart : 1,
      minDate: dateToday,
      step: 15,
      lang: 'fr',
      scrollMonth: false,
      scrollTime: false,
      scrollInput: false
    });

    $('[data-toggle=confirmation]').confirmation();

    $('body').on('click', '.btn-validate', function(e){
        e.preventDefault();
        var route = $(this).data('route');
        var promise = $.ajax({
            url: route,
            data: $(this).serialize(),
            method: 'PUT'
        });

        promise.done(function(data) {
          $('.td_'+data.id).html(data.msg);
        });
        promise.fail(function(jqXHR, textStatus, errorThrown) {
          var response = $.parseJSON(jqXHR.responseText);
          var options = {
            message: response.msg,
            buttons: [
              {text: 'Fermer', style: 'default', close: true}
            ]
          };

          eModal.alert(options);
        });
    });

    $(".select2").select2({
        theme: "classic",
        allowClear: true,
        placeholder: '',
        formatNoMatches: function () {
            return 'Aucun résultat';
        },
        formatSearching: function () {
            return 'Recherche...';
        }
    });

    $("#toTop").scrollToTop();

    $('body').on('click', '.modal-show', function(event){
      event.preventDefault();
      var options = {
        url: $(this).data('route'),
        title: $(this).data('title'),
        buttons: [
          {text: 'Fermer', style: 'default', close: true}
        ]
      };
      eModal.ajax(options);
    });

    if($('#circle-nbLimitPlayers').length > 0) {
      $("#circle-nbLimitPlayers").circliful();
    };

    if($('#circle-nbLimitPlayers-show').length > 0) {
      $("#circle-nbLimitPlayers-show").circliful();
    };

    if($('.colorChoice').length > 0) {
      $(".colorChoice").colorpicker();
    }

    if($('.summernote').length > 0) {
      $('.summernote').summernote({
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']]
        ]
      });
    }


    $('body').on('click', '.event-tab-2', function(e){
        e.preventDefault();
        $('.tab_2_coverage').html('');
        $('.loading').show();
        var slug = $(this).data('slug');

        var promise = $.ajax({
          url: Routing.generate('app_coverage_list_event_ajax', { slug: slug }),
          method: 'GET'
        });

        promise.done(function(data) {
          $('.loading').hide();
          $('.tab_2_coverage').html(data);
        });

        promise.fail(function(jqXHR, textStatus, errorThrown) {
          $('.loading').hide();
          var response = $.parseJSON(jqXHR.responseText);
          $('.tab_2_coverage').html('');
        });
    });
});
