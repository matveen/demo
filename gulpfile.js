var gulp         = require('gulp');
var gutil        = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');
var less         = require('gulp-less');
var concat       = require('gulp-concat');
var plumber      = require('gulp-plumber');
var source       = require('vinyl-source-stream');
var runSequence  = require('run-sequence');
var rename       = require('gulp-rename');
var uglify       = require("gulp-uglify");
var gulpif       = require("gulp-if");
var path         = require('path');
var cssmin       = require('gulp-cssmin');

var production   = process.env.NODE_ENV === 'production';

var config = {
  BOWER_DIR: 'app/Resources/vendors',
  SRC_DIR: 'app/Resources/assets',
  DIST_DIR: 'web/dist'
};

var uglifyOptions = {
  mangle: false,
  compress: {
    pure_getters: true,
    screw_ie8: true,
    warnings: false
  }
}

/*
 |--------------------------------------------------------------------------
 | Compile LESS stylesheets.
 |--------------------------------------------------------------------------
 */
gulp.task('less:frontend', function() {
  return gulp.src([
      config.BOWER_DIR + '/bootstrap/less/bootstrap.less',
      config.BOWER_DIR + '/font-awesome/less/font-awesome.less',
      config.BOWER_DIR + '/ionicons/less/ionicons.less',
      config.SRC_DIR   + '/less/frontend.less'
    ])
    .pipe(plumber())
    .pipe(less({
      paths: [
        path.join(__dirname, config.SRC_DIR + '/less')
      ]
    }))
    .pipe(concat('tmp.css'))
    .pipe(autoprefixer())
    .pipe(gulpif(production, cssmin()))
    .pipe(gulp.dest(config.SRC_DIR + '/less'));
});

gulp.task('less', [
  'less:frontend'
]);

/*
 |--------------------------------------------------------------------------
 | Concat CSS
 |--------------------------------------------------------------------------
 */
gulp.task('concat:css-app-vendors', function() {
  return gulp.src([
    config.BOWER_DIR + '/admin-lte/dist/css/AdminLTE.min.css',
    config.BOWER_DIR + '/admin-lte/dist/css/skins/skin-red.min.css',
    config.BOWER_DIR + '/jquery-ui/themes/ui-lightness/jquery-ui.min.css',
    config.BOWER_DIR + '/datetimepicker/jquery.datetimepicker.css',
    config.BOWER_DIR + '/select2/dist/css/select2.min.css',
    config.BOWER_DIR + '/fullcalendar/dist/fullcalendar.min.css',
    config.BOWER_DIR + '/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
    config.BOWER_DIR + '/summernote/dist/summernote.css',
    config.SRC_DIR   + '/js/circliful/css/jquery.circliful.css',
  ])
  .pipe(plumber())
  .pipe(concat('app_vendors.css'))
  .pipe(gulpif(production, cssmin()))
  .pipe(gulp.dest(config.DIST_DIR + '/css'));
});

gulp.task('concat:css-app', function() {
  return gulp.src([
    config.SRC_DIR + '/less/tmp.css'
  ])
  .pipe(plumber())
  .pipe(concat('app.css'))
  .pipe(gulpif(production, cssmin()))
  .pipe(gulp.dest(config.DIST_DIR + '/css'));
});

gulp.task('css', [
  'concat:css-app-vendors',
  'concat:css-app'
]);

/*
 |--------------------------------------------------------------------------
 | Concat js
 |--------------------------------------------------------------------------
 */
gulp.task('concat:js-app-vendors', function() {
  return gulp.src([
    config.BOWER_DIR + '/bootstrap/dist/js/bootstrap.min.js',
    config.BOWER_DIR + '/jquery-ui/jquery-ui.js',
    config.BOWER_DIR + '/jquery-ui/ui/i18n/datepicker-fr.js',
    config.BOWER_DIR + '/admin-lte/dist/js/app.min.js',
    config.BOWER_DIR + '/slimScroll/jquery.slimscroll.min.js',
    config.BOWER_DIR + '/bootstrap-confirmation2/bootstrap-confirmation.min.js',
    config.BOWER_DIR + '/moment/min/moment.min.js',
    config.BOWER_DIR + '/moment/locale/fr.js',
    config.BOWER_DIR + '/php-date-formatter/js/php-date-formatter.js',
    config.BOWER_DIR + '/datetimepicker/jquery.datetimepicker.js',
    config.BOWER_DIR + '/select2/dist/js/select2.full.min.js',
    config.BOWER_DIR + '/select2/dist/js/i18n/fr.js',
    config.BOWER_DIR + '/scrollToTop/jquery.scrollToTop.min.js',
    config.BOWER_DIR + '/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
    config.BOWER_DIR + '/emodal/dist/eModal.min.js',
    config.BOWER_DIR + '/summernote/dist/summernote.min.js',
    config.BOWER_DIR + '/summernote/dist/lang/summernote-fr-FR.min.js',
    config.SRC_DIR   + '/js/circliful/js/jquery.circliful.min.js'
  ])
  .pipe(plumber())
  .pipe(concat('app_vendors.js'))
  .pipe(gulpif(production, uglify(uglifyOptions)))
  .pipe(gulp.dest(config.DIST_DIR + '/js'));
});

gulp.task('concat:js-app-calendar', function() {
  return gulp.src([
      config.BOWER_DIR + '/moment/min/moment.min.js',
      config.BOWER_DIR + '/fullcalendar/dist/fullcalendar.min.js',
      config.BOWER_DIR + '/fullcalendar/dist/lang/fr.js'
  ])
  .pipe(plumber())
  .pipe(concat('app_calendar.js'))
  .pipe(gulpif(production, uglify(uglifyOptions)))
  .pipe(gulp.dest(config.DIST_DIR + '/js'));
});


gulp.task('concat:js-app', function() {
  return gulp.src([
    config.SRC_DIR + '/js/custom.js'
  ])
  .pipe(plumber())
  .pipe(concat('app.js'))
  .pipe(gulpif(production, uglify(uglifyOptions)))
  .pipe(gulp.dest(config.DIST_DIR + '/js'));
});

gulp.task('js', [
  'concat:js-app-vendors',
  'concat:js-app',
  'concat:js-app-calendar'
]);

/*
 |--------------------------------------------------------------------------
 | Copy assets to build dir
 |--------------------------------------------------------------------------
 */
gulp.task('copy:to-img', function() {
  return gulp.src([
      config.SRC_DIR  + '/img/**/*',
      config.BOWER_DIR + '/bootstrap-colorpicker/dist/img/**/*'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/img'));
});

gulp.task('copy:to-fonts', function() {
  return gulp.src([
      config.BOWER_DIR + '/bootstrap/dist/fonts/*',
      config.BOWER_DIR + '/font-awesome/fonts/*',
      config.BOWER_DIR +  '/ionicons/fonts/*'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/fonts'));
});

gulp.task('copy:to-css', function() {
  return gulp.src([
            config.SRC_DIR + '/js/leaflet/images/*'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/css'));
});

gulp.task('copy:to-css-font', function() {
  return gulp.src([
            config.BOWER_DIR + '/summernote/dist/font/*'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/css/font'));
});

gulp.task('copy:to-css-images', function() {
  return gulp.src([
      config.BOWER_DIR + '/jquery-ui/themes/base/images/*',
      config.BOWER_DIR + '/jquery-ui/themes/ui-lightness/images/*'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/css/images'));
});

gulp.task('copy:models', function() {
  return gulp.src([
      config.SRC_DIR  + '/models/*',
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/models'));
});

gulp.task('copy:to-js', function() {
  return gulp.src([
      config.BOWER_DIR + '/jquery/dist/jquery.min.js'
    ])
    .pipe(gulp.dest(config.DIST_DIR + '/js'));
});

gulp.task('copy', [
  'copy:to-img',
  'copy:to-fonts',
  'copy:to-css',
  'copy:to-css-font',
  'copy:to-css-images',
  'copy:models',
  'copy:to-js'
]);

/*
 |--------------------------------------------------------------------------
 | Watcher
 |--------------------------------------------------------------------------
 */
gulp.task('watch', function() {

  gulp.watch([
    config.SRC_DIR + '/less/**/*.less',
    config.SRC_DIR + '/less/*.less'
  ], ['less']);

  gulp.watch([
    config.SRC_DIR + '/css/**/*.css',
    config.SRC_DIR + '/css/*.css',
    config.SRC_DIR + '/less/*.css'
  ], ['concat:css-app']);

  gulp.watch([
    config.SRC_DIR + '/js/**/*.js'
  ], ['concat:js-app']);

  gulp.watch([
    config.SRC_DIR + '/img/**/*',
    config.SRC_DIR + '/img/*'
  ], ['copy:to-img']);

  gulp.watch([
    config.SRC_DIR + '/models/*'
  ], ['copy:models']);
});

/*
 |--------------------------------------------------------------------------
 | Parent tasks
 |--------------------------------------------------------------------------
 */
gulp.task('default', function(cb) {
  return runSequence(['less', 'css','js','copy'], 'watch', cb);
});

gulp.task('release', function(cb) {
  return runSequence(['less', 'css','js','copy'], cb);
});
