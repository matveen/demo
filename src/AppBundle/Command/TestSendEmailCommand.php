<?php

namespace AppBundle\Command;

use AppBundle\Utils\MailerManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestSendEmailCommand extends Command
{
    private $mailerManager;

    public function __construct(MailerManager $mailerManager)
    {
        parent::__construct();
        $this->mailerManager = $mailerManager;
    }

    protected function configure()
    {
        $this
            ->setName('app:test-send-email')
            ->setDescription('Test send email')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'email du destinataire obligatoire'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');

        $this->mailerManager->sendTest($email);
    }
}
