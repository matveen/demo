<?php

namespace AppBundle\Command;

use AppBundle\Utils\PictureSizeHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;

class GeneratePicturesSizeCommand extends Command
{
    private $logger;
    private $imageHandler;

    public function __construct(
        LoggerInterface $logger, 
        PictureSizeHandler $imageHandler
        )
    {
        parent::__construct();
        $this->logger = $logger;
        $this->imageHandler = $imageHandler;
    }

    protected function configure()
    {
        $this
            ->setName('app:generate:pictures')
            ->setDescription('Generate pictures to all format')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
        $this->logger->info('Start generate pictures in uploads folder');

        $finder = new Finder();
        $finder->files()->in('web/uploads');

        $size = iterator_count($finder);
        $batchSize = 20;
        $i = 1;

        $progress = new ProgressBar($output, $size);
        $progress->start();

        foreach ($finder as $file) {
            $output->writeln($file->getFilename());

            $this->imageHandler->processSizes('uploads/'.$file->getRelativePathname());
            
            if (($i % $batchSize) === 0) {
                $progress->advance($batchSize);
                $now = new \DateTime();
                $output->writeln(' of pictures generated ... | ' . $now->format('d-m-Y G:i:s'));
            }

            $i++;
        }

        $progress->finish();

        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
        $this->logger->info('End generate pictures');
    }
}
