<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use AppBundle\Entity\Participation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class ParticipationManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function register(User $user, Event $event)
    {
        $participation = new Participation();
        $participation->setUser($user);
        $participation->setEvent($event);

        $this->entityManager->persist($participation);
        $this->entityManager->flush();
    }

    public function unregister(Participation $participation)
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($participation);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return true;
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return false;
        }

        return false;
    }
}
