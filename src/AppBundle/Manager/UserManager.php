<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class UserManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function update()
    {
        $this->entityManager->flush();
    }

    public function remove(User $user)
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return new Response('user.flash.success.delete', 200);
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return new Response('user.flash.error.delete', 500);
        }

        return new Response('user.flash.error.delete', 500);
    }

    public function removeAvatar(User $user)
    {
        $this->entityManager->remove($user->getAvatar());
        $this->entityManager->flush();
    }
}
