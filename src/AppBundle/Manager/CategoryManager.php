<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class CategoryManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Category $category)
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    public function update()
    {
        $this->entityManager->flush();
    }

    public function remove(Category $category)
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($category);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return new Response('category.flash.success.delete', 200);
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return new Response('category.flash.error.delete', 500);
        }

        return new Response('category.flash.error.delete', 500);
    }
}
