<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class AddressManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Address $address)
    {
        $this->entityManager->persist($address);
        $this->entityManager->flush();
    }

    public function update()
    {
        $this->entityManager->flush();
    }

    public function remove(Address $address)
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($address);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return new Response('address.flash.success.delete', 200);
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return new Response('address.flash.error.delete', 500);
        }

        return new Response('address.flash.error.delete', 500);
    }
}
