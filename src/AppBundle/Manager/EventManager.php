<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class EventManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Event $event)
    {
        $this->entityManager->persist($event);
        $this->entityManager->flush();
    }

    public function update()
    {
        $this->entityManager->flush();
    }

    public function remove(Event $event)
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($event);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return new Response('event.flash.success.delete', 200);
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return new Response('event.flash.error.delete', 500);
        }

        return new Response('event.flash.error.delete', 500);
    }
}
