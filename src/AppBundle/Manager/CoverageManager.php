<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Coverage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class CoverageManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Coverage $coverage)
    {
        $this->entityManager->persist($coverage);
        $this->entityManager->flush();
    }

    public function update()
    {
        $this->entityManager->flush();
    }

    public function remove(Coverage $coverage)
    {
        try {

            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->remove($coverage);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();

            return new Response('coverage.flash.success.delete', 200);
            
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();

            return new Response('coverage.flash.error.delete', 500);
        }

        return new Response('coverage.flash.error.delete', 500);
    }
}
