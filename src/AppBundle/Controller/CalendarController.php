<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Coverage;
use AppBundle\Entity\Event;
use AppBundle\Form\Handler\CoverageFormHandler;
use AppBundle\Form\Type\CoverageType;
use AppBundle\Manager\CoverageManager;
use AppBundle\Model\Controller as Controller;
use AppBundle\Utils\ExportCsv;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/calendar")
 * @Security("has_role('ROLE_OFFICE')")
 */
class CalendarController extends Controller
{
    /**
     * @Route("", name="app_calendar_show")
     * @Method("GET")
     */
    public function showAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $events = $entityManager->getRepository('AppBundle:Event')
            ->getEvents();

        return $this->render('event/calendar.html.twig', [
            'events' => $events
        ]);
    }

    /**
     * @Route("/export-players/{slug}/{order}", name="app_calendar_event_export_players", requirements={"order": "rand|created_at_asc|created_at_desc"})
     * @Method({"GET"})
     */
    public function exportEventPlayersAction(Event $event, Request $request, ExportCsv $exportCsv, string $order)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $filters['eventID'] = $event->getId();
        $filters['order'] = $order;
        if ($event->getNbLimitPlayers()) { $filters['limit'] = $event->getNbLimitPlayers(); }

        $eventUsers = $entityManager->getRepository('AppBundle:Participation')->getUsersEvent($filters);
        if (count($eventUsers) == 0) {
            $this->addFlash('warning', 'event.label.no_players');
            return $this->redirectToRoute('app_event_index');
        }

        $users = new ArrayCollection();
        foreach($eventUsers as $eventUser) {
            $user = $entityManager->getReference('AppBundle:User', $eventUser['user_id']);
            if ($user) { $users->add($user); }
        }
        $exportCsv->process('players', $users, 'Participants_'.$event->getDateStart()->format('d-m-Y'));

        return new Response();
    }

   /**
     * @Route("/show-popup-export-players/{slug}", name="app_calendar_event_show_popup_export_players", options={"expose"=true})
     * @Method({"GET"})
     */
    public function showPopupExportPlayersAction(Event $event, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        return $this->render('event/_popup-export-players.html.twig', [
            'event' => $event
        ]);
    }

     /**
     * @Route("/next-user-events", name="app_calendar_event_next_user_events")
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     */
    public function nextUserEventsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $eventsArray = $entityManager->getRepository('AppBundle:Participation')
            ->getNextUserEvents($this->getUser()->getId());

        $events = new ArrayCollection();

        if (count($eventsArray) > 0) {
            foreach($eventsArray as $eventArray) {
                $event = $entityManager->getReference('AppBundle:Event', $eventArray['event_id']);
                $events->add($event);
            }
        }

        return $this->render('event/next-user-events.html.twig', [
            'events' => $events
        ]);
    }


}
