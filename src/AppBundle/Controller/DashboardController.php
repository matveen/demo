<?php

namespace AppBundle\Controller;

use AppBundle\Utils\DateHelper;
use AppBundle\Model\Controller as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard_homepage")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = [
            'users_count'      => $entityManager->getRepository('AppBundle:User')->countGlobal(),
            'categories_count' => $entityManager->getRepository('AppBundle:Category')->countGlobal(),
            'addresses_count'  => $entityManager->getRepository('AppBundle:Address')->countGlobal(),
            'events_count'     => $entityManager->getRepository('AppBundle:Event')->countGlobal(),
        ];

        return $this->render('dashboard/index.html.twig', $data);
    }
}
