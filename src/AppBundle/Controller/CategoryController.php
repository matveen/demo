<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\Handler\CategoryFormHandler;
use AppBundle\Form\Type\CategoryType;
use AppBundle\Form\Type\Filter\CategoryFilterType;
use AppBundle\Manager\CategoryManager;
use AppBundle\Model\Controller as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/categories")
 * @Security("has_role('ROLE_OFFICE')")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="app_category_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $filters = $this->get('session')->get('filters/category') ? $this->get('session')->get('filters/category') : [];

        $formFilters = $this->createForm(CategoryFilterType::class, $filters);
        $formFilters->handleRequest($request);
        if ($formFilters->isSubmitted() && $formFilters->isValid()) {
            $filters = $formFilters->getData();
        }
        $this->get('session')->set('filters/category', $filters);

        $query = $entityManager->getRepository('AppBundle:Category')
            ->getList($filters);
        $entities = $this->buildPaginator($query, $request->query->get('page', 1), Category::NUM_ITEMS);

        return $this->render('category/index.html.twig', [
            'entities' => $entities,
            'formFilters' => $formFilters->createView(),
        ]);
    }

    /**
     * @Route("/new", name="app_category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, CategoryFormHandler $formHandler)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'category.flash.success.create');
            
            return $this->redirectToRoute('app_category_index');
        }  

        return $this->render('category/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{slug}", name="app_category_edit")
     * @Method({"GET", "PUT"})
     */
    public function editAction(Request $request, Category $category, CategoryFormHandler $formHandler)
    {
        $form = $this->createForm(CategoryType::class, $category, [
            'action' => $this->generateUrl('app_category_edit', ['slug' => $category->getSlug()]),
            'method' => 'PUT'
        ]);

        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'category.flash.success.edit');

            return $this->redirectToRoute('app_category_edit', ['slug' => $category->getSlug()]);
        }

        return $this->render('category/edit.html.twig', [
            'form' => $form->createView(),
            'category' => $category
        ]);
    }

    /**
     * @Route("/{slug}", name="app_category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Category $category, CategoryManager $categoryManager)
    {
        $remover = $categoryManager->remove($category);
        
        if ($remover->getStatusCode() == 200) {
            $this->addFlash('success', $remover->getContent());

            return $this->redirectToRoute('app_category_index');
        } else {
            $this->addFlash('error', $remover->getContent());

            return $this->redirectToRoute('app_category_edit', [
                'slug' => $category->getSlug()
            ]);
        }
    }

    /**
     * @Route("/legend", name="app_category_legend")
     * @Method("GET")
     */
    public function legendAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository('AppBundle:Category')
            ->getList();

        return $this->render('category/_legend.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/remove/session/filters-categories", name="app_category_remove_filters")
     * @Method("DELETE")
     */
    public function removerFiltersAction(Request $request)
    {
        $this->get('session')->remove('filters/category');

        return $this->redirectToRoute('app_category_index');
    }
}
