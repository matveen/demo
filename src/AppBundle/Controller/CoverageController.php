<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Coverage;
use AppBundle\Entity\Event;
use AppBundle\Form\Handler\CoverageFormHandler;
use AppBundle\Form\Type\CoverageType;
use AppBundle\Manager\CoverageManager;
use AppBundle\Model\Controller as Controller;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/coverages")
 * @Security("has_role('ROLE_OFFICE')")
 */
class CoverageController extends Controller
{
    /**
     * @Route("/{slug}", name="app_coverages_event")
     * @Method({"GET", "POST"})
     */
    public function coveragesAction(Event $event, Request $request, CoverageFormHandler $formHandler)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $coverage = new Coverage();
        $coverage->setEvent($event);
        $form = $this->createForm(CoverageType::class, $coverage);
        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'coverage.flash.success.create');
            
            return $this->redirectToRoute('app_event_coverages', [
                'slug' => $event->getSlug()
            ]);
        } 

        $filters = [];
        $filters['event'] = $event;
        $coverages = $entityManager->getRepository('AppBundle:Coverage')
            ->getList($filters, 'DESC');

        return $this->render('event/coverages.html.twig', [
            'event' => $event,
            'coverages' => $coverages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/coverage/rm/{id}", name="app_coverages_event_remove", requirements={"id" = "\d+"})
     * @Method("DELETE")
     */
    public function deleteCoverageAction(Coverage $coverage, CoverageManager $coverageManager)
    {
        $event = $coverage->getEvent();

        $remover = $coverageManager->remove($coverage);
        
        if ($remover->getStatusCode() == 200) {
            $this->addFlash('success', $remover->getContent());

            return $this->redirectToRoute('app_event_coverages', [
                'slug' => $event->getSlug()
            ]);
        } else {
            $this->addFlash('error', $remover->getContent());

            return $this->redirectToRoute('app_event_coverages', [
                'slug' => $event->getSlug()
            ]);
        }
    }

    /**
     * @Route("/list-coverage-event-ajax/{slug}", name="app_coverage_list_event_ajax", options={"expose"=true})
     * @Method({"GET"})
     */
    public function coverageEventAjaxAction(Event $event, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        $entityManager = $this->getDoctrine()->getManager();
        
        $filters = [];
        $filters['event'] = $event;
        $coverages = $entityManager->getRepository('AppBundle:Coverage')
            ->getList($filters, 'DESC');

        return $this->render('event/_list-coverages.html.twig', [
            'coverages' => $coverages,
            'canDel'    => false,
        ]);
    }
}
