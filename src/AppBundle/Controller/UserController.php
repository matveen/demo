<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Handler\UserFormHandler;
use AppBundle\Form\Type\CompanyUserType;
use AppBundle\Form\Type\ConditionsType;
use AppBundle\Form\Type\Filter\UserFilterType;
use AppBundle\Form\Type\UserType;
use AppBundle\Manager\UserManager;
use AppBundle\Model\Controller as Controller;
use AppBundle\Utils\ExportCsv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/users")
 * @Security("has_role('ROLE_OFFICE')")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="app_users_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, ExportCsv $exportCsv)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $export = false;
        $filters = $this->get('session')->get('filters/users') ? $this->get('session')->get('filters/users') : [];
        $filters = array_merge($this->getCurrentUserFilters(), $filters);

        $formFilters = $this->createForm(UserFilterType::class, $filters);
        $formFilters->handleRequest($request);
        if ($formFilters->isSubmitted() && $formFilters->isValid()) {
            $filters = $formFilters->getData();
            if ($formFilters->get('export')->isClicked()) { $export = true; }
        }
        $this->get('session')->set('filters/users', $filters);

        $query = $entityManager->getRepository('AppBundle:User')->getList($filters);
        $entities = $this->buildPaginator($query, $request->query->get('page', 1), User::NUM_ITEMS);

        if (true === $export && $entities->getTotalItemCount() == 0) {
            $this->addFlash('warning', 'no_datas');
            return $this->redirectToRoute('app_users_index');
        } elseif (true === $export) {
            $exportCsv->process('user', $entities, 'utilisateurs');
        }

        return $this->render('user/index.html.twig', [
            'entities' => $entities,
            'formFilters' => $formFilters->createView(),
        ]);
    }

    /**
     * @Route("/new", name="app_users_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, UserFormHandler $formHandler)
    {
        $form = $this->createForm(UserType::class, null, [
            'validation_groups' => ['Registration']
        ]);

        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'user.flash.success.create');
            
            return $this->redirectToRoute('app_users_index');
        }  

        return $this->render('user/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="app_users_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "PUT"})
     * @Security("userL.hasAccess(user)")
     */
    public function editAction(Request $request, User $userL, UserFormHandler $formHandler)
    {
        $form = $this->createForm(UserType::class, $userL, [
            'action' => $this->generateUrl('app_users_edit', ['id' => $userL->getId()]),
            'validation_groups' => ['Profile'],
            'method' => 'PUT'
        ]);

        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'user.flash.success.edit');

            return $this->redirectToRoute('app_users_edit', ['id' => $userL->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $userL
        ]);
    }

    /**
     * @Route("/validate/{id}", name="app_users_validate", requirements={"id" = "\d+"})
     * @Method("PUT")
     * @Security("userL.hasAccess(user)")
     */
    public function validateAction(User $userL, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $value = $userL->getEnabled() ? false : true;
        $userL->setEnabled($value);

        $entityManager->flush();

        $response = $value ? '<span class="badge bg-green"><i class="fa fa-fw fa-check"></i>' : '<span class="badge bg-red"><i class="fa fa-fw fa-remove"></i>';

        return new JsonResponse([
            'msg' => $response,
            'id' => $userL->getId()
        ], 200);
    }

    /**
     * @Route("/{id}", name="app_users_delete", requirements={"id" = "\d+"})
     * @Method("DELETE")
     * @Security("userL.hasAccess(user)")
     */
    public function deleteAction(User $userL, UserManager $userManager)
    {
        $remover = $userManager->remove($userL);
        
        if ($remover->getStatusCode() == 200) {
            $this->addFlash('success', $remover->getContent());

            return $this->redirectToRoute('app_users_index');
        } else {
            $this->addFlash('danger', $remover->getContent());

            return $this->redirectToRoute('app_users_edit', [
                'id' => $userL->getId()
            ]);
        }
    }

    /**
     * @Route("/remove/session/filters-users", name="app_users_remove_filters")
     * @Method("DELETE")
     */
    public function removerFiltersAction(Request $request)
    {
        $this->get('session')->remove('filters/users');

        return $this->redirectToRoute('app_users_index');
    }

    /**
     * @Route("/show/{id}", name="app_users_show_ajax", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function showAction(Request $request, User $user)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        return $this->render('user/_show_ajax.html.twig', [
            'user' => $user
        ]);
    }
}
