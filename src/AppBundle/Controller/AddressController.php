<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Form\Handler\AddressFormHandler;
use AppBundle\Form\Type\AddressType;
use AppBundle\Form\Type\Filter\AddressFilterType;
use AppBundle\Manager\AddressManager;
use AppBundle\Model\Controller as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/addresses")
 * @Security("has_role('ROLE_OFFICE')")
 */
class AddressController extends Controller
{
    /**
     * @Route("/", name="app_address_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $filters = $this->get('session')->get('filters/address') ? $this->get('session')->get('filters/address') : [];

        $formFilters = $this->createForm(AddressFilterType::class, $filters);
        $formFilters->handleRequest($request);
        if ($formFilters->isSubmitted() && $formFilters->isValid()) {
            $filters = $formFilters->getData();
        }
        $this->get('session')->set('filters/address', $filters);

        $query = $entityManager->getRepository('AppBundle:Address')
            ->getList($filters);
        $entities = $this->buildPaginator($query, $request->query->get('page', 1), Address::NUM_ITEMS);

        return $this->render('address/index.html.twig', [
            'entities' => $entities,
            'formFilters' => $formFilters->createView(),
        ]);
    }

    /**
     * @Route("/new", name="app_address_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, AddressFormHandler $formHandler)
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);
        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'address.flash.success.create');
            
            return $this->redirectToRoute('app_address_index');
        }  

        return $this->render('address/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="app_address_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "PUT"})
     */
    public function editAction(Request $request, Address $address, AddressFormHandler $formHandler)
    {
        $form = $this->createForm(AddressType::class, $address, [
            'action' => $this->generateUrl('app_address_edit', ['id' => $address->getId()]),
            'method' => 'PUT'
        ]);

        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'address.flash.success.edit');

            return $this->redirectToRoute('app_address_edit', ['id' => $address->getId()]);
        }

        return $this->render('address/edit.html.twig', [
            'form' => $form->createView(),
            'address' => $address
        ]);
    }

    /**
     * @Route("/{id}", name="app_address_delete", requirements={"id" = "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Address $address, AddressManager $addressManager)
    {
        $remover = $addressManager->remove($address);
        
        if ($remover->getStatusCode() == 200) {
            $this->addFlash('success', $remover->getContent());

            return $this->redirectToRoute('app_address_index');
        } else {
            $this->addFlash('error', $remover->getContent());

            return $this->redirectToRoute('app_address_edit', [
                'id' => $address->getId()
            ]);
        }
    }

    /**
     * @Route("/remove/session/filters-addresses", name="app_address_remove_filters")
     * @Method("DELETE")
     */
    public function removerFiltersAction(Request $request)
    {
        $this->get('session')->remove('filters/address');

        return $this->redirectToRoute('app_address_index');
    }
}
