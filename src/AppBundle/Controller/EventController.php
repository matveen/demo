<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Coverage;
use AppBundle\Entity\Event;
use AppBundle\Entity\Participation;
use AppBundle\Form\Handler\CoverageFormHandler;
use AppBundle\Form\Handler\EventFormHandler;
use AppBundle\Form\Type\CoverageType;
use AppBundle\Form\Type\EventType;
use AppBundle\Form\Type\Filter\EventFilterType;
use AppBundle\Manager\CoverageManager;
use AppBundle\Manager\EventManager;
use AppBundle\Manager\ParticipationManager;
use AppBundle\Model\Controller as Controller;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/events")
 * @Security("has_role('ROLE_OFFICE')")
 */
class EventController extends Controller
{
    /**
     * @Route("/", name="app_event_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $filters = $this->get('session')->get('filters/event') ? $this->get('session')->get('filters/event') : [];

        $formFilters = $this->createForm(EventFilterType::class, $filters);
        $formFilters->handleRequest($request);
        if ($formFilters->isSubmitted() && $formFilters->isValid()) {
            $filters = $formFilters->getData();
        }
        $this->get('session')->set('filters/event', $filters);

        $query = $entityManager->getRepository('AppBundle:Event')
            ->getList($filters);
        $entities = $this->buildPaginator($query, $request->query->get('page', 1), Event::NUM_ITEMS);

        return $this->render('event/index.html.twig', [
            'entities' => $entities,
            'formFilters' => $formFilters->createView(),
        ]);
    }

    /**
     * @Route("/new", name="app_event_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, EventFormHandler $formHandler)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'event.flash.success.create');
            
            return $this->redirectToRoute('app_event_index');
        }  

        return $this->render('event/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{slug}", name="app_event_edit")
     * @Method({"GET", "PUT"})
     */
    public function editAction(Request $request, Event $event, EventFormHandler $formHandler)
    {
        $form = $this->createForm(EventType::class, $event, [
            'action' => $this->generateUrl('app_event_edit', ['slug' => $event->getSlug()]),
            'method' => 'PUT'
        ]);

        if ($formHandler->handle($form, $request)) {
            $this->addFlash('success', 'event.flash.success.edit');

            return $this->redirectToRoute('app_event_edit', ['slug' => $event->getSlug()]);
        }

        return $this->render('event/edit.html.twig', [
            'form' => $form->createView(),
            'event' => $event
        ]);
    }

    /**
     * @Route("/{slug}", name="app_event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Event $event, EventManager $eventManager)
    {
        $remover = $eventManager->remove($event);
        
        if ($remover->getStatusCode() == 200) {
            $this->addFlash('success', $remover->getContent());

            return $this->redirectToRoute('app_event_index');
        } else {
            $this->addFlash('error', $remover->getContent());

            return $this->redirectToRoute('app_event_edit', [
                'slug' => $event->getSlug()
            ]);
        }
    }

    /**
     * @Route("/validate/{slug}", name="app_event_validate")
     * @Method("PUT")
     */
    public function validateAction(Event $event, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $value = $event->getEnabled() ? false : true;
        $event->setEnabled($value);

        $entityManager->flush();

        $response = $value ? '<span class="badge bg-green"><i class="fa fa-fw fa-check"></i>' : '<span class="badge bg-red"><i class="fa fa-fw fa-remove"></i>';

        return new JsonResponse([
            'msg' => $response,
            'id' => $event->getId()
        ], 200);
    }

    /**
     * @Route("/show/{slug}", name="app_event_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Event $event)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $filters = [];
        $filters['event'] = $event;
        $coverages = $entityManager->getRepository('AppBundle:Coverage')
            ->getList($filters, 'DESC');

        $isUserRegisterOnEvent = $entityManager->getRepository('AppBundle:Participation')
            ->isUserRegisterOnEvent($this->getUser()->getId(), $event->getId());

        $filters = [];
        $filters['eventID'] = $event->getId();
        $filters['order'] = 'created_at_asc';
        
        // PLAYERS IN
        if ($event->getNbLimitPlayers()) {
            $filters['limit'] = $event->getNbLimitPlayers();
        }
        $playersArray = $entityManager->getRepository('AppBundle:Participation')
            ->getUsersEvent($filters);

        $players = new ArrayCollection();
        if (count($playersArray) > 0) {
            foreach($playersArray as $playerArray) {
                $player = $entityManager->getReference('AppBundle:User', $playerArray['user_id']);
                if ($player) {
                    $players->add($player);
                }
            }
        }

        // PLAYERS WAIT
        $playersWait = new ArrayCollection();
        if ($event->getNbLimitPlayers()) {
            $filters['limitstart'] = $event->getNbLimitPlayers();
            $filters['limit'] = 1000;
            $playersWaitArray = $entityManager->getRepository('AppBundle:Participation')
                ->getUsersEvent($filters);

            if (count($playersWaitArray) > 0) {
                foreach($playersWaitArray as $playerWaitArray) {
                    $player = $entityManager->getReference('AppBundle:User', $playerWaitArray['user_id']);
                    if ($player) {
                        $playersWait->add($player);
                    }
                }
            }
        }

        return $this->render('event/show.html.twig', [
            'event' => $event,
            'coverages' => $coverages,
            'isUserRegisterOnEvent' => $isUserRegisterOnEvent,
            'players' => $players,
            'playersWait' => $playersWait
        ]);
    }

   /**
     * @Route("/event-register-user/{slug}", name="app_event_register_user")
     * @Method("GET")
     */
    public function registerUserAction(Request $request, Event $event, ParticipationManager $participationManager)
    {
        $entityManager = $this->getDoctrine()->getManager();

        // Check limit to register
        if (false === $event->canRegister()) {
            $this->addFlash('error', 'event.flash.success.register_too_late');

            return $this->redirectToRoute('app_event_show', [
                'slug' => $event->getSlug()
            ]);
        }

        // Check if user already register
        $isUserRegisterOnEvent = $entityManager->getRepository('AppBundle:Participation')
            ->isUserRegisterOnEvent($this->getUser()->getId(), $event->getId());

        if (false !== $isUserRegisterOnEvent) {
            $this->addFlash('error', 'event.flash.success.register_already');

            return $this->redirectToRoute('app_event_show', [
                'slug' => $event->getSlug()
            ]);
        }

        $waitList = false;
        if ($event->getNbLimitPlayers()) {
            $nbPlayers = count($event->getEventsUser());
            if ($nbPlayers >= $event->getNbLimitPlayers()) {
                $waitList = true;
            }
        }

        $registered = false;

        if ((true === $event->getPrivate() &&
            true === $this->getUser()->isMember($event->getDateStart())) || 
            false === $event->getPrivate()
        ) {
            $participationManager->register($this->getUser(), $event);

            $registered = true;
        }

        if (true === $registered) {
            if (true === $waitList) {
                $this->addFlash('warning', 'event.flash.success.register_wait');
            } else {
                $this->addFlash('success', 'event.flash.success.register');
            }
        } else {
            $this->addFlash('error', 'event.flash.success.register_error');
        }

        return $this->redirectToRoute('app_event_show', [
            'slug' => $event->getSlug()
        ]);
    }

    /**
     * @Route("/event-unregister-user/{slug}", name="app_event_unregister_user")
     * @Method("GET")
     */
    public function unregisterUserAction(Request $request, Event $event, ParticipationManager $participationManager)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $participation = $entityManager->getRepository('AppBundle:Participation')
            ->findOneBy(array(
                'user' => $this->getUser(),
                'event' => $event
            ));
        ;

        if ($participation) {
            $participationManager->unregister($participation);

            $this->addFlash('success', 'event.flash.success.unregister');
        }

        return $this->redirectToRoute('app_event_show', [
            'slug' => $event->getSlug()
        ]);
    }

    /**
     * @Route("/remove/session/filters-events", name="app_event_remove_filters")
     * @Method("DELETE")
     */
    public function removerFiltersAction(Request $request)
    {
        $this->get('session')->remove('filters/event');

        return $this->redirectToRoute('app_event_index');
    }
}
