<?php

namespace AppBundle\Entity\EntityInterface;

interface AddressInterface
{
    public function getFullAddressText();
    public function getAddress();
}
