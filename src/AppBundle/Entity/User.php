<?php

namespace AppBundle\Entity;

use AppBundle\Utils\Slugger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_users")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    const NUM_ITEMS        = 10;
    const ROLE_USER        = 'ROLE_USER';
    const ROLE_OFFICE      = 'ROLE_OFFICE';
    const ROLE_ADMIN       = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10, nullable=true)
     * @Assert\Length(
     *       max=10,
     *       maxMessage="length.max"
     * )
     */
    private $gender;

   /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"Registration", "Profile"})
     * @Assert\Length(
     *       min=2,
     *       minMessage="length.min",
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"Registration", "Profile"})
     * @Assert\Length(
     *       min=2,
     *       minMessage="length.min",
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $firstname;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Avatar", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true, name="avatar_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $avatar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"Registration", "Profile"})
     */
    private $birthday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDateSubscription", type="date", nullable=true)
     */
    private $endDateSubscription;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_mobile", type="string", length=45, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"Registration", "Profile"})
     * @Assert\Length(
     *       max=45,
     *       maxMessage="length.max",
     *       groups={"Registration", "Profile"}
     * )
     */
    private $phoneMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="information", type="text", nullable=true)
     */
    private $information;

   /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Event", mappedBy="director", cascade={"persist"})
     */
    private $directorEvents;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Participation", mappedBy="user", cascade={"remove", "persist"})
    */
    private $userEvents;

   /**
     * @var string
     *
     * @ORM\Column(name="quote", type="string", length=255, nullable=true)
     * @Assert\Length(
     *       min=2,
     *       minMessage="length.min",
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $quote;

   /**
     * @var string
     *
     * @ORM\Column(name="winamax", type="string", length=255, nullable=true)
     * @Assert\Length(
     *       min=2,
     *       minMessage="length.min",
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $winamax;

    public function __construct()
    {
        parent::__construct();
        $this->roles = array(static::ROLE_USER);
        $this->directorEvents = new ArrayCollection();
        $this->userEvents = new ArrayCollection();
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        // check age birthday 
        $date = new \DateTime(); 
        if ($this->getBirthday() && ($date->format('Y') - $this->getBirthday()->format('Y')) < 18) {
            $context->buildViolation('age')
                ->atPath('birthday')
                ->addViolation()
            ;
        }
    }

    public function setSimpleRole($role)
    {
        return $this->setRoles([$role]);
    }

    public function getSimpleRole()
    {
        $roles = $this->getRoles();
        if (!$roles) {
            return null;
        }

        return current($roles);
    }

    /**
     * @param bool $onlyKeys
     *
     * @return array
     */
    public static function getRolesChoices($onlyKeys = false)
    {
        $roles = array(
            self::ROLE_USER         => 'ROLE_USER',
            self::ROLE_OFFICE       => 'ROLE_OFFICE',
            self::ROLE_ADMIN        => 'ROLE_ADMIN',
            self::ROLE_SUPER_ADMIN  => 'ROLE_SUPER_ADMIN'
        );

        return true === $onlyKeys ? array_keys($roles) : $roles;
    }

    public function displayName($gender = false)
    {
        $fullname = '';
        if(true === $gender && null !== $this->getGender()) { 
            $fullname.= $this->getGender().' ';
        }
        $fullname.= $this->getFirstname().' '.strtoupper($this->getLastname());
        
        return $fullname;
    }

    public function displaySlugName()
    {
        return Slugger::slugify($this->getLastname().'-'.$this->getFirstname());
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set avatar
     *
     * @param \AppBundle\Entity\Avatar $avatar
     *
     * @return User
     */
    public function setAvatar(\AppBundle\Entity\Avatar $avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return \AppBundle\Entity\Avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set endDateSubscription
     *
     * @param \DateTime $endDateSubscription
     *
     * @return User
     */
    public function setEndDateSubscription($endDateSubscription)
    {
        $this->endDateSubscription = $endDateSubscription;

        return $this;
    }

    /**
     * Get endDateSubscription
     *
     * @return \DateTime
     */
    public function getEndDateSubscription()
    {
        return $this->endDateSubscription;
    }

    /**
     * Set phoneMobile
     *
     * @param string $phoneMobile
     *
     * @return User
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phoneMobile = $phoneMobile;

        return $this;
    }

    /**
     * Get phoneMobile
     *
     * @return string
     */
    public function getPhoneMobile()
    {
        return $this->phoneMobile;
    }

    public function getWebPathUserAvatar()
    {
        if ($this->getAvatar()) {
            return $this->getAvatar()->getWebPath();
        }

        if ($this->getGender() == "M.") {
            return '/dist/img/avatar.png';
        }

        return '/dist/img/avatar-woman.png';
    }

    public function isMember(\DateTime $toDate = null)
    {
        if (null === $this->getEndDateSubscription()) {

            return false;
        }

        if (null === $toDate) {
            $now = new \DateTime();
            if ($this->getEndDateSubscription()->format('Ymd') >= $now->format('Ymd')) {

                return true;
            }
        } else {
            if ($this->getEndDateSubscription()->format('Ymd') >= $toDate->format('Ymd')) {

                return true;
            }
        }

        return false;
    }

    /**
     * Set information
     *
     * @param string $information
     *
     * @return User
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    public function hasAccess(\AppBundle\Entity\User $user)
    {
        if ($user->getSimpleRole() != "ROLE_SUPER_ADMIN" && 
            $this->getSimpleRole() == 'ROLE_SUPER_ADMIN') {
            return false;
        }

        return true;
    }

    /**
     * Add directorEvent
     *
     * @param \AppBundle\Entity\Event $directorEvent
     *
     * @return User
     */
    public function addDirectorEvent(\AppBundle\Entity\Event $directorEvent)
    {
        $this->directorEvents[] = $directorEvent;

        return $this;
    }

    /**
     * Remove directorEvent
     *
     * @param \AppBundle\Entity\Event $directorEvent
     */
    public function removeDirectorEvent(\AppBundle\Entity\Event $directorEvent)
    {
        $this->directorEvents->removeElement($directorEvent);
    }

    /**
     * Get directorEvents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectorEvents()
    {
        return $this->directorEvents;
    }

    /**
     * Add userEvent
     *
     * @param \AppBundle\Entity\Participation $userEvent
     *
     * @return User
     */
    public function addUserEvent(\AppBundle\Entity\Participation $userEvent)
    {
        $this->userEvents[] = $userEvent;

        return $this;
    }

    /**
     * Remove userEvent
     *
     * @param \AppBundle\Entity\Participation $userEvent
     */
    public function removeUserEvent(\AppBundle\Entity\Participation $userEvent)
    {
        $this->userEvents->removeElement($userEvent);
    }

    /**
     * Get userEvents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserEvents()
    {
        return $this->userEvents;
    }

    /**
     * Set quote
     *
     * @param string $quote
     *
     * @return User
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Get quote
     *
     * @return string
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * Set winamax
     *
     * @param string $winamax
     *
     * @return User
     */
    public function setWinamax($winamax)
    {
        $this->winamax = $winamax;

        return $this;
    }

    /**
     * Get winamax
     *
     * @return string
     */
    public function getWinamax()
    {
        return $this->winamax;
    }
}
