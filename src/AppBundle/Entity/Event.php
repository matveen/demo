<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\EventRepository")
 * @ORM\Table(name="app_event")
 * @ORM\HasLifecycleCallbacks
 */
class Event
{
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to_published", type="datetime", nullable=true)
     */
    private $dateToPublished;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_limit_participation", type="datetime", nullable=true)
     */
    private $dateLimitParticipation;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $slug;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

   /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default" = 0})
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumn( nullable=true, name="address_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumn( nullable=true, name="categori_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $category;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_limit_players", type="integer", nullable=true)
     * @Assert\Type(
     *      type="integer", 
     *      message="integer.not_valid")
     */
    private $nbLimitPlayers;

   /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Coverage", mappedBy="event", cascade={"remove", "persist"}))
     */
    private $coverages;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="directorEvents", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $director;

   /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", options={"default": 0})
     */
    private $private;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Participation", mappedBy="event", cascade={"remove", "persist"}))
     */
    protected $eventsUser;

    /**
     * @var string
     *
     * @ORM\Column(name="information", type="text", nullable=true)
     */
    private $information;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="text", nullable=true)
     */
    private $resume;

    public function __construct()
    {
        $this->enabled = false;
        $this->coverages = new ArrayCollection();
        $this->private = false;
        $this->eventsUser = new ArrayCollection();
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getDateEnd() && ($this->getDateStart() >= $this->getDateEnd()) ) {
            $context->buildViolation('event.error.end_at_superior')
                ->atPath('dateEnd')
                ->addViolation()
            ;
        }

        if (null !== $this->getDateLimitParticipation() && $this->getDateLimitParticipation() > $this->getDateStart()) {
            $context->buildViolation('event.error.date_limit_superior_date_start')
                ->atPath('dateLimitParticipation')
                ->addViolation()
            ;
        }

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Event
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Event
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateToPublished
     *
     * @param \DateTime $dateToPublished
     *
     * @return Event
     */
    public function setDateToPublished($dateToPublished)
    {
        $this->dateToPublished = $dateToPublished;

        return $this;
    }

    /**
     * Get dateToPublished
     *
     * @return \DateTime
     */
    public function getDateToPublished()
    {
        return $this->dateToPublished;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Event
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Event
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Event
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Event
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Event
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Event
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set nbLimitPlayers
     *
     * @param integer $nbLimitPlayers
     *
     * @return Event
     */
    public function setNbLimitPlayers($nbLimitPlayers)
    {
        $this->nbLimitPlayers = $nbLimitPlayers;

        return $this;
    }

    /**
     * Get nbLimitPlayers
     *
     * @return integer
     */
    public function getNbLimitPlayers()
    {
        return $this->nbLimitPlayers;
    }

    /**
     * Add coverage
     *
     * @param \AppBundle\Entity\Coverage $coverage
     *
     * @return Event
     */
    public function addCoverage(\AppBundle\Entity\Coverage $coverage)
    {
        $this->coverages[] = $coverage;

        return $this;
    }

    /**
     * Remove coverage
     *
     * @param \AppBundle\Entity\Coverage $coverage
     */
    public function removeCoverage(\AppBundle\Entity\Coverage $coverage)
    {
        $this->coverages->removeElement($coverage);
    }

    /**
     * Get coverages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoverages()
    {
        return $this->coverages;
    }

    /**
     * Set director
     *
     * @param \AppBundle\Entity\User $director
     *
     * @return Event
     */
    public function setDirector(\AppBundle\Entity\User $director = null)
    {
        $this->director = $director;

        return $this;
    }

    /**
     * Get director
     *
     * @return \AppBundle\Entity\User
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Set private
     *
     * @param boolean $private
     *
     * @return Event
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get private
     *
     * @return boolean
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Add eventsUser
     *
     * @param \AppBundle\Entity\Participation $eventsUser
     *
     * @return Event
     */
    public function addEventsUser(\AppBundle\Entity\Participation $eventsUser)
    {
        $this->eventsUser[] = $eventsUser;

        return $this;
    }

    /**
     * Remove eventsUser
     *
     * @param \AppBundle\Entity\Participation $eventsUser
     */
    public function removeEventsUser(\AppBundle\Entity\Participation $eventsUser)
    {
        $this->eventsUser->removeElement($eventsUser);
    }

    /**
     * Get eventsUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventsUser()
    {
        return $this->eventsUser;
    }

    /**
     * Set dateLimitParticipation
     *
     * @param \DateTime $dateLimitParticipation
     *
     * @return Event
     */
    public function setDateLimitParticipation($dateLimitParticipation)
    {
        $this->dateLimitParticipation = $dateLimitParticipation;

        return $this;
    }

    /**
     * Get dateLimitParticipation
     *
     * @return \DateTime
     */
    public function getDateLimitParticipation()
    {
        return $this->dateLimitParticipation;
    }

    public function displayName()
    {
        $name = $this->getDateStart()->format('d/m/Y H:i');

        if ($this->getCategory()) {
            $name.= ' - '.$this->getCategory()->getName();
        }

        $name.= ' - '.$this->getTitle();

        return $name;
    }

    public function displayCalendarName()
    {
        $name = '';

        if ($this->getCategory()) {
            $name.= strtoupper(substr($this->getCategory()->getName(), 0 ,4).'. ');
        }

        $name.= ' - '.$this->getTitle();

        return $name;
    }

    public function getPlayersRate(int $nbCurrentPlayers = null)
    {
        if ($this->getNbLimitPlayers() != 0 || null !== $this->getNbLimitPlayers()) {
            $rate = ($nbCurrentPlayers * 100 ) / $this->getNbLimitPlayers();

            return round($rate, 2);
        }

        return null;
    }

    /**
     * Set information
     *
     * @param string $information
     *
     * @return Event
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    public function canRegister()
    {
        if (!$this->getDateLimitParticipation()) {
            return true;
        }

        $now = new \DateTime();

        if ($now <= $this->getDateLimitParticipation()) {
            return true;
        }

        return false;
    }

    /**
     * Set resume
     *
     * @param string $resume
     *
     * @return Event
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }
}
