<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AvatarRepository")
 * @ORM\Table(name="app_avatars")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Avatar
{
    const PATH_FOLDER_FILE = "/uploads/avatar";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $file;

    /**
     * @var File $tmpFile
     *
     * @Assert\File(
     *     maxSize="4M",
     *     mimeTypes = {"image/jpeg", "image/png", "image/pjpeg", "image/gif"}
     * )
     * @Vich\UploadableField(mapping="user_avatar", fileNameProperty="file")
     *
     */
    protected $tmpFile;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Avatar
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Avatar
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getWebPath()
    {
        if (null !== $this->file) {
            return self::PATH_FOLDER_FILE.'/'.$this->file;
        }

        return null;
    }

    public function setTmpFile($file)
    {
        $this->tmpFile = $file;

        if (!empty($file)) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getTmpFile()
    {
        return $this->tmpFile;
    }
}
