<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\AddressInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AddressRepository")
 * @ORM\Table(name="app_address")
 * @ORM\HasLifecycleCallbacks
 */
class Address implements AddressInterface
{
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="street_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $streetName;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", length=3, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *       max=3,
     *       maxMessage="length.max"
     * )
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="complement", type="string", length=255, nullable=true)
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $complement;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=5, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Type(
     *      type="numeric", 
     *      message="numeric.not_valid")
     * @Assert\Length(
     *       min=5,
     *       minMessage="length.min",
     *       max=5,
     *       maxMessage="length.max"
     * )
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *       max=255,
     *       maxMessage="length.max"
     * )
     */
    private $city;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="decimal", precision=18, scale=12, nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="decimal", precision=18, scale=12, nullable=true)
     */
    private $longitude;

   /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Event", mappedBy="address", cascade={"persist"})
     */
    private $events;

    public function __construct()
    {
        $this->events  = new ArrayCollection();
    }

    public function getFullAddressText()
    {
        return trim(implode(' ', array_filter(array(
            $this->getNumber(),
            $this->getStreetName(),
            $this->getZipcode(),
            $this->getCity(),
            'FRANCE'
        ))));
    }

    public function getAddress()
    {
        return sprintf(
            '%s %s, %s %s, %s',
            $this->getNumber(),
            $this->getStreetName(),
            $this->getZipcode(),
            $this->getCity(),
            'FRANCE'
        );
    }

    public function getLocalAddress()
    {
        return sprintf(
            '%s %s, %s %s',
            $this->getNumber(),
            $this->getStreetName(),
            $this->getZipcode(),
            mb_strtoupper($this->getCity())
        );
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set streetName
     *
     * @param string $streetName
     *
     * @return Address
     */
    public function setStreetName($streetName)
    {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * Get streetName
     *
     * @return string
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Address
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set complement
     *
     * @param string $complement
     *
     * @return Address
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * Get complement
     *
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Address
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Address
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Address
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    public function isFullAddress()
    {
        if (!$this->getStreetName()) {
            return false;
        }

        if (!$this->getNumber() || $this->getNumber() == 0) {
            return false;
        }

        if (!$this->getZipcode() || !$this->getCity()) {
            return false;
        }

        return true;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Address
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
