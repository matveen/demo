<?php

namespace AppBundle\Utils;

class DateHelper
{
    public static function explodeDateToDatabase(string $date)
    {
      $arrayDate = explode('/', $date);

      return $arrayDate[2].'-'.$arrayDate[1].'-'.$arrayDate[0];
    }

    public static function prePath(string $date)
    {
        $dateObject = new \DateTime($date);

        return $dateObject->format('Y/m');
    }

    public static function getDateObject(string $date)
    {
        $dateObject = new \DateTime($date);

        return $dateObject;
    }
}
