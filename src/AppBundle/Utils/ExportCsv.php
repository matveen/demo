<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Company;
use AppBundle\Entity\Folder;
use AppBundle\Entity\User;
use League\Csv\Writer;
use Symfony\Component\Translation\TranslatorInterface;

class ExportCsv
{
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function process(string $context, $entities, string $outputName = 'export')
    {
        if (null === $context || null === $entities) {
            throw new \RuntimeException('Invalid parameters for export');
        }

        try {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->setDelimiter(';');
            $csv->setEnclosure('"');

            $csv->insertOne( $this->getHeaders($context) );

            foreach ($entities as $entity) {
                $csv->insertOne( $this->getMapRow($entity, $context) );
            }

            $csv->output($outputName.'_'.date('d-m-Y').'.csv');

             return true;
        } catch(\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function getHeaders(string $context)
    {
        if  ($context == 'user') {

            return [
                $this->translator->trans('user.label.lastname'),
                $this->translator->trans('user.label.firstname'),
                $this->translator->trans('user.label.birthday'),
                $this->translator->trans('user.label.email'),
                $this->translator->trans('user.label.phone_mobile'),
                $this->translator->trans('user.label.end_date_subscription')
            ];
        } elseif  ($context == 'players') {

            return [
                $this->translator->trans('user.label.lastname'),
                $this->translator->trans('user.label.firstname'),
                $this->translator->trans('user.label.email'),
                $this->translator->trans('user.label.phone_mobile')
            ];
        }

        return null;
    }

    private function getMapRow($entity, string $context)
    {
        if ($entity instanceof User && $context == "user") {

            return [
                $entity->getLastname(),
                $entity->getFirstname(),
                $entity->getBirthday()->format('d/m/Y'),
                $entity->getEmail(),
                $entity->getPhoneMobile(),
                $entity->getEndDateSubscription() ? $entity->getEndDateSubscription()->format('d/m/Y') : ''
            ];
        }  elseif ($entity instanceof User && $context == "players") {

            return [
                $entity->getLastname(),
                $entity->getFirstname(),
                $entity->getEmail(),
                $entity->getPhoneMobile()
            ];
        }

        return null;
    }
}
