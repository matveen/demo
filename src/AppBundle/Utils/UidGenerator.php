<?php

namespace AppBundle\Utils;

class UidGenerator
{
    public function generate(string $prefix = "X", bool $doStrrev = true)
    {
        $pre = "";
        if ($prefix != "") {
            $pre = (true === $doStrrev) ? strrev($prefix) : $prefix;
        }
        $uid = $pre . $this->getSeed() . rand(0, 999);

        return $uid;
    }

    /**
     * @return string
     */
    public static function getSeed()
    {
        $m = microtime(true);

        return sprintf(
            '%d%03d%05d%03d',
            date('y') - 12, // nombres d'années depuis 2012
            date('z'),
            ((int)date('H') * 3600) + ((int)date('i') * 60) + (int)date('s'),
            (int)floor(($m - floor($m)) * 1000)
        );
    }

    /**
     * @return string
     */
    public static function getPassword()
    {
        $s = substr(str_shuffle(str_repeat("012345678-9ABCEZY-WFGHIJMNOPQRS-Tabcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        return self::getSeed().$s; 
    }
}
