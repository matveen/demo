<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Company;
use Symfony\Component\Templating\EngineInterface;

class MailerManager
{
    protected $engine;
    protected $mailer;
    protected $appTitle;
    protected $mailerFrom;
    protected $mailerTo;
    protected $urlSite;

    public function __construct(
        EngineInterface $engine,
        \Swift_Mailer $mailer,
        $appTitle, 
        $mailerFrom,
        $mailerTo,
        $urlSite)
    {
        $this->engine     = $engine;
        $this->mailer     = $mailer;
        $this->appTitle   = $appTitle;
        $this->mailerFrom = $mailerFrom;
        $this->mailerTo   = $mailerTo;
        $this->urlSite    = $urlSite;
    }

    public function sendTest($email)
    {
        $from = $this->mailerFrom;
        $to = $email;
        $subject = "[".$this->appTitle."] Test envoi email";

        $bodyTextPlain = $this->engine->render('mailer/test.txt.twig', ['email' => $email]);
        $bodyHtml = $this->engine->render('mailer/test.html.twig', ['email' => $email]);

        $this->sendMessage($from, $to, $subject, $bodyTextPlain, $bodyHtml);
    }

    public function sendUserPassword(\AppBundle\Entity\User $user, string $plainPassword)
    {
        $from = $this->mailerFrom;
        $to = $user->getEmail();
        $subject = "[".$this->appTitle."] - Identifiants de connexion";

        $bodyTextPlain = $this->engine->render('mailer/user-password.txt.twig', [
            'user'   => $user,
            'plainPassword'   => $plainPassword
        ]);
        $bodyHtml = $this->engine->render('mailer/user-password.html.twig', [
            'user'   => $user,
            'plainPassword'   => $plainPassword
        ]);

        $this->sendMessage($from, $to, $subject, $bodyTextPlain, $bodyHtml);
    }

    public function sendUserInfos(\AppBundle\Entity\User $user)
    {
        $from = $this->mailerFrom;
        $to = $user->getEmail();
        $subject = "[".$this->appTitle."] Bienvenue !";

        $bodyTextPlain = $this->engine->render('mailer/user-infos.txt.twig', [
            'user'   => $user
        ]);
        $bodyHtml = $this->engine->render('mailer/user-infos.html.twig', [
            'user'   => $user
        ]);

        $this->sendMessage($from, $to, $subject, $bodyTextPlain, $bodyHtml);
    }

    protected function sendMessage($from, $to, $subject, $bodyTextPlain, $bodyHtml, $attachedFile = null)
    {
        $mail = \Swift_Message::newInstance();

        $mail
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($bodyTextPlain)
            ->addPart($bodyHtml, 'text/html');
            if (null !== $attachedFile) {
               $mail->attach(\Swift_Attachment::fromPath($attachedFile), "application/octet-stream"); 
            }

        $this->mailer->send($mail);
    }
}
