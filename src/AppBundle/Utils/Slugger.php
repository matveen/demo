<?php

namespace AppBundle\Utils;

use Gedmo\Sluggable\Util\Urlizer;

class Slugger
{
    public static function slugify($string, $separator = '-')
    {
        $string = Urlizer::unaccent($string);
        $string = strtolower($string);
        $slug = preg_replace('/([^a-z0-9]+)/', $separator, $string);        

        return $slug;
    }
}
