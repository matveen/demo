<?php

namespace AppBundle\Utils;

use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Psr\Log\LoggerAwareTrait;

/*
 * To use : $this->get('app.image_size_handler')->processSizes($pathToPicture);
 */

class PictureSizeHandler
{
    use LoggerAwareTrait;

    /**
     * @var DataManager
     */
    protected $dataManager;

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * @param DataManager     $dataManager
     * @param FilterManager   $filterManager
     * @param CacheManager    $cacheManager
     * @param SignerInterface $signer
     */
    public function __construct(
        DataManager $dataManager,
        FilterManager $filterManager,
        CacheManager $cacheManager
    ) {
        $this->dataManager = $dataManager;
        $this->filterManager = $filterManager;
        $this->cacheManager = $cacheManager;
    }

    public function processSizes($path, $filters = null)
    {
        if (null === $filters) {
            $filters = $this->getFilters();
        }

        if (count($filters > 0)) {
            foreach ($filters as $filter) {
                $this->processSize($filter, $path);
            }
        }
    }

    public function processSize($filter, $path)
    {
        if ($this->cacheManager->isStored($path, $filter)) {
            return;
        }

        try {
            $binary = $this->dataManager->find($filter, $path);
        } catch (NotLoadableException $e) {
            if ($this->logger) {
                $this->logger->err($e->getMessage());
            }
        }

        $this->cacheManager->store(
            $this->filterManager->applyFilter($binary, $filter),
            $path,
            $filter
        );
    }

    protected function getFilters()
    {
        $filters = array_keys($this->filterManager->getFilterConfiguration()->all());

        return $filters;
    }
}
