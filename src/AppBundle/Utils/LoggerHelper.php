<?php

namespace AppBundle\Utils;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerHelper
{
    protected $kernelRootDir;

    public function __construct($kernelRootDir)
    {
        $this->kernelRootDir = $kernelRootDir;
    }

    public function log(string $file, string $channel)
    {
        $log = new Logger($channel);
        $dateFormat = "Y-m-d H:i:s";
        $outputFormatter = "[%datetime%] [%channel%] [%level_name%] %message% %context% %extra%\n";
        $formatter = new LineFormatter($outputFormatter, $dateFormat);
        $streamHandler = new StreamHandler($this->kernelRootDir . '/var/logs/'.$file.'.log');
        $streamHandler->setFormatter($formatter);
        $log->pushHandler($streamHandler);

        return $log;
    }
}
