<?php

namespace AppBundle\Form\Handler;

use AppBundle\Form\Handler\BaseFormHandler;
use AppBundle\Manager\EventManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class EventFormHandler extends BaseFormHandler
{
    private $eventManager;
    private $translator;

    public function __construct(
        EventManager $eventManager,
        TranslatorInterface $translator)
    {
        $this->eventManager = $eventManager;
        $this->translator = $translator;
    }

    public function handle(FormInterface $form, Request $request) 
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $this->addMessage('danger', $this->translator->trans('flash.form.error'), $request);
                
                return false;
            } 

            $eventValid = $form->getData();

            if (!$eventValid->getId()) {
                $this->eventManager->create($eventValid);
            } else {
                $this->eventManager->update($eventValid);
            }

            return true;
        }
    }
}
