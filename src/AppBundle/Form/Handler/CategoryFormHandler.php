<?php

namespace AppBundle\Form\Handler;

use AppBundle\Form\Handler\BaseFormHandler;
use AppBundle\Manager\CategoryManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class CategoryFormHandler extends BaseFormHandler
{
    private $categoryManager;
    private $translator;

    public function __construct(
        CategoryManager $categoryManager,
        TranslatorInterface $translator)
    {
        $this->categoryManager = $categoryManager;
        $this->translator = $translator;
    }

    public function handle(FormInterface $form, Request $request) 
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $this->addMessage('danger', $this->translator->trans('flash.form.error'), $request);
                
                return false;
            } 

            $categoryValid = $form->getData();

            if (!$categoryValid->getId()) {
                $this->categoryManager->create($categoryValid);
            } else {
                $this->categoryManager->update($categoryValid);
            }

            return true;
        }
    }
}
