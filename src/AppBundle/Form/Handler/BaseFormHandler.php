<?php 

namespace AppBundle\Form\Handler;

use Symfony\Component\HttpFoundation\Request;

class BaseFormHandler 
{
    protected function addMessage(string $type, string $message, Request $request)
    {
        $request->getSession()->getFlashBag()->add($type, $message);
    } 
}
