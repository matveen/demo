<?php

namespace AppBundle\Form\Handler;

use AppBundle\Form\Handler\BaseFormHandler;
use AppBundle\Manager\CoverageManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class CoverageFormHandler extends BaseFormHandler
{
    private $coverageManager;
    private $translator;

    public function __construct(
        CoverageManager $coverageManager,
        TranslatorInterface $translator)
    {
        $this->coverageManager = $coverageManager;
        $this->translator = $translator;
    }

    public function handle(FormInterface $form, Request $request) 
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $this->addMessage('danger', $this->translator->trans('flash.form.error'), $request);
                
                return false;
            } 

            $coverageValid = $form->getData();

            if (!$coverageValid->getId()) {
                $this->coverageManager->create($coverageValid);
            } else {
                $this->coverageManager->update($coverageValid);
            }

            return true;
        }
    }
}
