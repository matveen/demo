<?php

namespace AppBundle\Form\Handler;

use AppBundle\Form\Handler\BaseFormHandler;
use AppBundle\Manager\AddressManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AddressFormHandler extends BaseFormHandler
{
    private $addressManager;
    private $translator;

    public function __construct(
        AddressManager $addressManager,
        TranslatorInterface $translator)
    {
        $this->addressManager = $addressManager;
        $this->translator = $translator;
    }

    public function handle(FormInterface $form, Request $request) 
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $this->addMessage('danger', $this->translator->trans('flash.form.error'), $request);
                
                return false;
            } 

            $addressValid = $form->getData();

            if (!$addressValid->getId()) {
                $this->addressManager->create($addressValid);
            } else {
                $this->addressManager->update($addressValid);
            }

            return true;
        }
    }
}
