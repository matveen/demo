<?php

namespace AppBundle\Form\Handler;

use AppBundle\Form\Handler\BaseFormHandler;
use AppBundle\Manager\UserManager;
use AppBundle\Utils\MailerManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserFormHandler extends BaseFormHandler
{
    private $userManager;
    private $translator;
    private $mailerManager;

    public function __construct(
        UserManager $userManager, 
        TranslatorInterface $translator, 
        MailerManager $mailerManager)
    {
        $this->userManager   = $userManager;
        $this->translator    = $translator;
        $this->mailerManager = $mailerManager;
    }

    public function handle(FormInterface $form, Request $request) 
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $this->addMessage('danger', $this->translator->trans('flash.form.error'), $request);
                
                return false;
            } 

            $userValid = $form->getData();

            if (($plainPassword = $form->get('plainPassword')->getData())) {
                $this->mailerManager->sendUserPassword($userValid, $plainPassword);
            }

            if (!$userValid->getId()) {
                $this->userManager->create($userValid);
            } else {

                if (isset($form["avatar"]["remove_pic"]) && $form["avatar"]["remove_pic"]->getData()) {
                    $this->userManager->removeAvatar($userValid);
                }

                $this->userManager->update($userValid);
            }

            return true;
        }
    }
}
