<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Address;
use AppBundle\EventListener\AddRemoveCheckboxFieldSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'    => 'address.label.name',
                'required' => false
            ])
            ->add('complement', TextType::class, [
                'label'    => 'address.label.complement',
                'required' => false
            ])
            ->add('number', NumberType::class, [
                'label'    => 'address.label.number',
                'required' => true
            ])
            ->add('streetName', TextType::class, [
                'label'    => 'address.label.streetname',
                'required' => true
            ])
            ->add('zipcode', TextType::class, [
                'attr'     => [
                    'class' => 'autocomplete_zipcode input_zipcode',
                    'maxlength' => 5,
                    'autocomplete' => 'off'
                ],
                'label'    => 'address.label.zipcode',
                'required' => true
            ])
            ->add('city', TextType::class, [
                'attr'     => [
                    'class' => 'autocomplete_city input_city',
                    'autocomplete' => 'off'
                ],
                'label'    => 'address.label.city',
                'required' => true
            ])
            ->add('latitude', HiddenType::class, [
                'label'    => 'profile.show.address.latitude',
                'required' => false
            ])
            ->add('longitude', HiddenType::class, [
                'label'    => 'address.label.longitude',
                'required' => false
            ])
        ;

         $builder->addEventSubscriber(new AddRemoveCheckboxFieldSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Address'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_address';
    }
}
