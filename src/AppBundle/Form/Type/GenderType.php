<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenderType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'gender.mister' => 'M.',
                'gender.lady'   => 'Mme',
                'gender.miss'   => 'Mlle'
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
