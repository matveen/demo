<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Repository\AddressRepository;
use AppBundle\Entity\Repository\CategoryRepository;
use AppBundle\Entity\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', EntityType::class, [
                'label'    => 'event.label.address',
                'attr'          => ['class' => 'select2'],
                'class'         => 'AppBundle:Address',
                'choice_label'  => 'name',
                'query_builder' => function (AddressRepository $er) {
                    return $er->getAddresses();
                },
                'required'     => false
            ])
            ->add('category', EntityType::class, [
                'label'    => 'event.label.category',
                'attr'          => ['class' => 'select2'],
                'class'         => 'AppBundle:Category',
                'choice_label'  => 'name',
                'query_builder' => function (CategoryRepository $er) {
                    return $er->getCategories();
                },
                'required'     => false
            ])
            ->add('title', TextType::class, [
                'label'    => 'event.label.title',
                'required' => true
            ])
            ->add('enabled', CheckboxType::class, [
                'label'    => 'event.label.enabled',
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'class' => 'summernote'
                ],
                'label'    => 'event.label.description',
                'required' => false
            ])
            ->add('dateToPublished', DateType::class, [
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy H:mm',
                'label'    => 'event.label.dateToPublished',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('dateStart', DateType::class, [
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy H:mm',
                'label'    => 'event.label.dateStart',
                'required' => true,
                'widget' => 'single_text'
            ])
            ->add('dateLimitParticipation', DateType::class, [
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy H:mm',
                'label'    => 'event.label.dateLimitParticipation',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('dateEnd', DateType::class, [
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy H:mm',
                'label'    => 'event.label.dateEnd',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('nbLimitPlayers', IntegerType::class, [
                'label'    => 'event.label.nb_limit_players',
                'required' => false
            ])
            ->add('director', EntityType::class, [
                'label'    => 'event.label.director',
                'attr'          => ['class' => 'select2'],
                'class'         => 'AppBundle:User',
                'choice_label'  => 'displayName',
                'query_builder' => function (UserRepository $er) {
                    return $er->getUsers('ROLE_OFFICE');
                },
                'required'     => false
            ])
            ->add('private', CheckboxType::class, [
                'label'    => 'event.label.is_private',
                'required' => false
            ])
            ->add('resume', TextareaType::class, [
                'attr' => [
                    'class' => 'summernote'
                ],
                'label'    => 'event.label.resume',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Event'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_event';
    }
}
