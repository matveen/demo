<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\AvatarType;
use AppBundle\Form\Type\GenderType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('current_password')
            ->add('firstname', TextType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.firstname'
                ],
                'label'  => 'profile.label.firstname',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email',
                'required' => true
            ])
            ->add('gender', GenderType::class, [
                'label' => 'profile.label.gender',
                'placeholder' => 'profile.label.gender',
                'required' => true
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.lastname'
                ],
                'label'    => 'profile.label.lastname',
                'required' => true
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options'     => ['label' => 'form.password'],
                'invalid_message'   => 'fos_user.password.mismatch',
                'second_options'    => ['label' => 'form.password_confirmation'],
                'type'              => PasswordType::class,
                'validation_groups' => ['Profile'],
                'required'          => false
            ])
            ->add('birthday', DateType::class, [
                'attr' => [
                    'class' => 'datepicker'
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'profile.label.birthday',
                'required' => true,
                'widget' => 'single_text'
            ])
            ->add('phoneMobile', TextType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.phone_mobile'
                ],
                'label'    => 'profile.label.phone_mobile',
                'required' => true
            ])
            ->add('avatar', AvatarType::class, [
                'required' => false
            ])
            ->add('quote', TextType::class, [
                'label'    => 'profile.label.quote',
                'required' => false
            ])
            ->add('winamax', TextType::class, [
                'label'    => 'profile.label.winamax',
                'required' => false
            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
            'translation_domain' => 'FOSUserBundle'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}
