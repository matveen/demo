<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\AvatarType;
use AppBundle\Form\Type\GenderType;
use AppBundle\Form\Type\UserRolesType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enabled', CheckboxType::class, [
                'label'    => 'profile.label.enabled',
                'required' => false
            ])
            ->add('gender', GenderType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.gender'
                ],
                'label' => 'profile.label.gender',
                'placeholder' => 'profile.label.gender',
                'required' => false
            ])
            ->add('firstname', TextType::class, [
                'label'    => 'profile.label.firstname',
                'required' => true
            ])
            ->add('lastname', TextType::class, [
                'label'    => 'profile.label.lastname',
                'required' => true
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options'     => ['label' => 'form.password'],
                'invalid_message'   => 'fos_user.password.mismatch',
                'second_options'    => ['label' => 'form.password_confirmation'],
                'type'              => PasswordType::class,
                'validation_groups' => ['Profile'],
                'required' => false
            ])
            ->add('username', TextType::class, [
                'label' => 'form.username',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email',
                'required' => true
            ])
            ->add('roles', UserRolesType::class, [
                'label' => 'profile.label.role',
                'property_path' => 'simpleRole'
            ])
            ->add('birthday', DateType::class, [
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'profile.label.birthday',
                'required' => true,
                'widget' => 'single_text'
            ])
            ->add('endDateSubscription', DateType::class, [
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'profile.label.end_date_subscription',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('phoneMobile', TextType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.phone_mobile'
                ],
                'label'    => 'profile.label.phone_mobile',
                'required' => true
            ])
            ->add('avatar', AvatarType::class, [
                'required' => false
            ])
            ->add('information', TextareaType::class, [
                'attr' => [
                    'rows' => 3
                ],
                'label' => 'profile.label.information',
                'required' => false
            ])
            ->add('quote', TextType::class, [
                'label'    => 'profile.label.quote',
                'required' => false
            ])
            ->add('winamax', TextType::class, [
                'label'    => 'profile.label.winamax',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
            'translation_domain' => 'FOSUserBundle'
        ]);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_users_user_registration';
    }
}
