<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\GenderType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label'  => 'profile.label.firstname',
                'required'     => true
            ])
            ->add('gender', GenderType::class, [
                'label' => 'profile.label.gender',
                'placeholder' => 'profile.label.gender',
                'required' => true
            ])
            ->add('lastname', TextType::class, [
                'label'    => 'profile.label.lastname',
                'required' => true
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options'     => ['label' => 'form.password'],
                'invalid_message'   => 'fos_user.password.mismatch',
                'second_options'    => ['label' => 'form.password_confirmation'],
                'type'              => PasswordType::class,
                'validation_groups' => ['Registration'],
                'required'          => false,
            ])
            ->add('birthday', DateType::class, [
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => false
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'profile.label.birthday',
                'required' => true,
                'widget' => 'single_text'
            ])
            ->add('phoneMobile', TextType::class, [
                'attr' => [
                    'placeholder' => 'profile.label.phone_mobile'
                ],
                'label'    => 'profile.label.phone_mobile',
                'required' => true
            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
            'translation_domain' => 'FOSUserBundle'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
