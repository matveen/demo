<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Avatar;
use AppBundle\EventListener\AddRemoveCheckboxFieldSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AvatarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tmpFile', FileType::class, [
                'attr' => ['class' => 'inputPicture'],
                'by_reference' => false,
                'label'    => 'profile.label.avatar_img',
                'required' => false
            ])
        ;

        $builder->addEventSubscriber(new AddRemoveCheckboxFieldSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Avatar'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_avatar';
    }
}
