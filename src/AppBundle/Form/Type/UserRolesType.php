<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserRolesType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices'  => $this->getRoles(),
            'required' => true
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    private function getRoles()
    {
        $roles = User::getRolesChoices();
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user || $user->getSimpleRole() != 'ROLE_SUPER_ADMIN') {
            unset($roles['ROLE_SUPER_ADMIN']);
        }

        return $roles;
    }
}
