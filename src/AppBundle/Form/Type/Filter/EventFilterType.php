<?php

namespace AppBundle\Form\Type\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'event.label.title',
                'required' => false
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'event.label.enabled',
                'required' => false,
                'empty_data' => null,
                'placeholder' => 'choice.title',
                'choices' => [
                    'choice.no' => 0,
                    'choice.yes' => 1
                ]
            ])
            ->add('private', ChoiceType::class, [
                'label' => 'event.label.is_private',
                'required' => false,
                'empty_data' => null,
                'placeholder' => 'choice.title',
                'choices' => [
                    'choice.no' => 0,
                    'choice.yes' => 1
                ]
            ])
            ->add('dateStart', DateType::class, [
                'attr' => [
                    'class' => 'datepicker'
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'event.label.dateStart',
                'required' => false,
                'widget' => 'single_text'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET'
        ]);
    }
}
