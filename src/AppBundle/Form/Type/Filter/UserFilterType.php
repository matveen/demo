<?php

namespace AppBundle\Form\Type\Filter;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, [
                'label' => 'profile.label.lastname',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'profile.label.email',
                'required' => false
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'profile.label.enabled',
                'required' => false,
                'empty_data' => null,
                'placeholder' => 'choice.title',
                'choices' => [
                    'choice.no' => 0,
                    'choice.yes' => 1
                ]
            ])
            ->add('subscribers', CheckboxType::class, [
                'label'    => 'title.block.subscribers',
                'required' => false
            ])
            ->add('createdAt', DateType::class, [
                'attr' => [
                    'class' => 'datepicker'
                ],
                'format' => 'dd/MM/yyyy',
                'label'    => 'profile.label.date.registration',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('export', SubmitType::class, [
                'attr'     => ['class' => "btn btn-block btn-default btn-flat mt10"],
                'label'    => 'the_action.filter_export_list'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'translation_domain' => 'FOSUserBundle',
            'companies' => []
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_users_filter';
    }
}
