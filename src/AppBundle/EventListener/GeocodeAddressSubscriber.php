<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;

use AppBundle\Entity\Address;

class GeocodeAddressSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return array(
            Events::preUpdate,
            Events::prePersist
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->setLocation($entity);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->setLocation($entity);
    }

   protected function setLocation($entity)
    {
        if (!$entity instanceof Address) {
            return false;
        }

        $text = trim($entity->getFullAddressText());

        if (0 === strlen($text)) {
            return false;
        }

        $location = $this->geocodeAddress($text);

        if (is_array($location)) {
            $entity->setLatitude($location[0]);
            $entity->setLongitude($location[1]);

            return true;
        }

        return false;
    }

    protected function geocodeAddress($text)
    {

        $curl     = new \Ivory\HttpAdapter\CurlHttpAdapter();
        $geocoder = new \Geocoder\Provider\GoogleMaps($curl, 'fr_FR', 'France', true);

        try {
            $result = $geocoder->geocode($text);
            $address = $result->first();

            return array(
                $address->getLatitude(), 
                $address->getLongitude()
            );

        } catch (\Exception $e) {}

        return null;
    }
}
