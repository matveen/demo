<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Avatar;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AddRemoveCheckboxFieldSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData'
        );
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($data instanceof Avatar) {
            if (null !== $data && null !== $data->getId() && null !== $data->getFile()) {
                $form->add('remove_pic', CheckboxType::class, [
                    'label'    => 'the_action.remove_file',
                    'required' => false,
                    'mapped'   => false
                ]);
            }
        } else {
            return;
        }
    }
}
