<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('displayStatus', array($this, 'displayStatus')),
            new \Twig_SimpleFunction('displayStatusBullet', array($this, 'displayStatusBullet')),
            new \Twig_SimpleFunction('displayPhone', array($this, 'displayPhone')),
            new \Twig_SimpleFunction('display_date_end_subscription', array($this, 'displayDateEndSubscription')),
            new \Twig_SimpleFunction('display_widget_color', array($this, 'displayWidgetColor')),
            new \Twig_SimpleFunction('display_age', array($this, 'displayAge')),
            new \Twig_SimpleFunction('display_label_class', array($this, 'displayLabelClass')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('html', [$this, 'html'], ['is_safe' => ['html']])
        );
    }

    public function html($html)
    {
        return $html;
    }

    public function displayStatus(bool $status)
    {
        if (true === $status) {
            return '<span class="badge bg-green"><i class="fa fa-fw fa-check"></i></span>';
        }

        return '<span class="badge bg-red"><i class="fa fa-fw fa-remove"></i></span>';
    }

    public function displayStatusBullet(bool $status)
    {
        if (true === $status) {
            return '<img src="/dist/img/green.png" />';
        }

        return '<img src="/dist/img/red.png" />';
    }

    public function displayPhone(string $phone = null)
    {
        if (null === $phone) {
            return;
        }

        $txt = wordwrap($phone, 2, " ", 1);

        return $txt;
    }

    public function displayDateEndSubscription(\DateTime $date)
    {
        if (null === $date) {
            return '~';
        }
        $now = new \DateTime();
        $interval = $now->diff($date);

        if (1 == $interval->invert) {
            $status = 'danger';
        } elseif (0 == $interval->invert && $interval->format('%m') == 0 && $interval->format('%d') < 30) {
            $status = 'warning';
        } else {
            $status = 'success';
        }

        return '<span class="label label-'.$status.'">'.$date->format('d/m/Y').'</span>';
    }

    public function displayWidgetColor(\DateTime $date)
    {
        if (null === $date) {
            return $color =  'red';
        }

        $now = new \DateTime();
        $interval = $now->diff($date);

        if (1 == $interval->invert) {
            $color = 'red';
        } elseif (0 == $interval->invert && $interval->format('%m') == 0 && $interval->format('%d') < 30) {
            $color = 'orange';
        } else {
            $color = 'green';
        }

       return $color;
    }

    public function displayAge(\DateTime $date)
    {
        return (int) ((time() - strtotime($date->format('Y-m-d'))) / 3600 / 24 / 365).' ans';
    }

    public function displayLabelClass($rate)
    {
        if ($rate >= 0 && $rate < 60) {
            return 'success';
        } elseif($rate >= 60 && $rate < 85) {
            return 'warning';
        } 

        return 'danger';
    }

    public function getName()
    {
        return 'app_extension';
    }
}
