<?php

namespace AppBundle\Model;

use AppBundle\Entity\User;
use AppBundle\Utils\FileManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as ParentController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Controller extends ParentController
{
    protected function buildPaginator($query, $page = 1, $numItems = 10)
    {
        $paginator  = $this->get('knp_paginator');

        $entities = $paginator->paginate($query, $page, $numItems);

        return $entities;
    }

    protected function getCurrentUserFilters()
    {
        $filters = [];
        $filters['user_role'] = $this->getUser()->getSimpleRole();

        return $filters;
    }
}
