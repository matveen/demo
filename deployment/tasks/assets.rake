namespace :assets do

    desc "Compress assets in a local file"
    task :compile do
        run_locally do   
            execute "rm -rf web/dist/*"       
            execute "npm run release"
            execute "tar zcvf assets.tgz web/dist/"
            execute "mv assets.tgz web/dist/"
        end
    end

    desc "Copy assets to server"
    task :copy do
        on roles(:web) do
          upload! "web/dist/assets.tgz", "#{release_path}/assets.tgz"
          execute "cd #{release_path}; tar zxvf assets.tgz; rm assets.tgz"
        end
    end

    desc "Clean local dist"
    task :clean do
        run_locally do   
            execute "rm -rf web/dist/assets.tgz"
        end
    end

    task deploy: %w(compile copy clean)
end