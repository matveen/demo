require 'yaml'

namespace :config do
    desc "Upload parameters file"
    task :upload_params do
        on roles (:app) do
            execute "mkdir -p #{shared_path}/app/config"

            config_options = YAML.load_file("app/config/parameters_#{fetch(:stage)}.yml")
            config_options['parameters']['assets_version'] = "#{release_timestamp}"
            File.open("app/config/parameters_#{fetch(:stage)}.yml", 'wb') {|f| f.write config_options.to_yaml } #Store
 
            upload! StringIO.new(File.read("app/config/parameters_#{fetch(:stage)}.yml")),
            "#{shared_path}/app/config/parameters.yml"
         end
    end

    desc "Apply database migrations"
    task :apply_database_migrations do
        invoke 'symfony:console', 'doctrine:migrations:migrate', '--no-interaction'
    end

    desc "clear cache prod"
    task :cache_clear do
        invoke 'symfony:console', 'cache:clear', '--env=prod'
    end

 end