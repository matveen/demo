# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'appdemo'
set :repo_url, 'git@bitbucket.org:matveen/demo.git'

set :composer_install_flags, '--dev --prefer-dist --no-interaction --optimize-autoloader'

set :tmp_dir, "/home/mathieu-d/tmp"

set :scm, :git
set :log_level, :debug

# Symfony settings
set :bin_path, "bin"
set :var_path, "var"
set :log_path, fetch(:var_path) + "/logs"
set :cache_path, fetch(:var_path) + "/cache"
set :session_path, fetch(:var_path) + "/sessions"
set :symfony_console_path, fetch(:bin_path) + "/console"

set :log_path, fetch(:app_path) + "/logs"
set :cache_path, fetch(:app_path) + "/cache"

set :linked_files, %w{app/config/parameters.yml}
set :linked_dirs, fetch(:linked_dirs, []).push('var/logs', 'var/sessions', 'web/uploads', 'web/media')

set :keep_releases, 3

set :controllers_to_clear, ["app_*.php", "config.php"]

set :file_permissions_paths, ['var/cache', 'var/logs']
set :file_permissions_users, ['www-data']

after 'deploy:starting', 'composer:install_executable'
before "deploy:check:linked_files", 'config:upload_params'
#before "deploy:updated", "deploy:set_permissions:acl"
after "deploy:updated", "config:apply_database_migrations"

after 'deploy:updated', 'assets:deploy'